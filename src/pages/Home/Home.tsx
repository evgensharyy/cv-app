import React from 'react';
import Button from '../../components/Button/Button';
import PhotoBox from '../../components/PhotoBox/PhotoBox';
import './Home.style.scss'

function Home() {
  return (
    <>
      <div className='home'>
        <img className='home-image' src={require('../../assets/images/image_2.png')} alt='' />
        <img className='home-overlay' src={require('../../assets/images/Rectangle_16.png')} alt='' />
      </div>
      <div className='content'>
        <PhotoBox name="John Doe" avatar={require('../../assets/images/user-avatar.png')} sxWidth={163} sxHeight={163} fontSize={36}>
          <h3 className='photoBox_name' style={{ fontSize: '45px'  }}>John Doe</h3>
          <h3 className='photoBox-title'>Programmer. Creative. Innovator</h3>
          <p className='photoBox-info'>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque </p>
          <a href={`inner`}>
            <Button text='Know more' />
          </a>
        </PhotoBox>
      </div>
    </>
  );
}

export default Home;
