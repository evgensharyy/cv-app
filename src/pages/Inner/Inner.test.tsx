import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Inner from './Inner';

describe('<Inner />', () => {
  test('it should mount', () => {
    render(<Inner />);
    
    const inner = screen.getByTestId('Inner');

    expect(inner).toBeInTheDocument();
  });
});