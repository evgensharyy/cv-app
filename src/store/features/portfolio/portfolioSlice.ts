import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface PortfolioState {
    filter: string
}

const initialState: PortfolioState = {
    filter: '*'
}

export const portfolioSlice = createSlice({
    name: 'portfolio',
    initialState,
    reducers: {
      setFilter: (state, action: PayloadAction<string>) => {
        state.filter = action.payload
      }
    },
  })
  
  // Action creators are generated for each case reducer function
  export const { setFilter } = portfolioSlice.actions
  
  export default portfolioSlice.reducer