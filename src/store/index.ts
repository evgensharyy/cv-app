import { configureStore, combineReducers, PreloadedState } from '@reduxjs/toolkit'
import portfolioReducer from './features/portfolio/portfolioSlice'

export const store = configureStore({
  reducer: {
    portfolio: portfolioReducer
  }
})

const rootReducer = combineReducers({
  portfolio: portfolioReducer
})

export function setupStore(preloadedState?: PreloadedState<RootState>) {
  return configureStore({
    reducer: rootReducer,
    preloadedState
  })
}

export type RootState = ReturnType<typeof store.getState>

export type AppStore = ReturnType<typeof setupStore>

export type AppDispatch = typeof store.dispatch