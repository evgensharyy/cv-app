import React from 'react';
import styles from './Button.module.scss'

interface ButtonProps {
  className?: string,
  icon?: React.ReactNode,
  text?: string,
  onClick?: () => void;
}

function Button({className, icon, text, onClick}: ButtonProps) {
  return (
    <span onClick={onClick} className={`${styles.button} ${className ? className : ''}`}>
      {icon}
      {text}
    </span>
  );
}

export default Button;