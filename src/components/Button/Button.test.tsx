import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Button from './Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

describe('<Button />', () => {
  test('it should mount', () => {
    render(<Button icon={<FontAwesomeIcon icon="chevron-left" />} text="Go back" />);
    
    const button = screen.getByTestId('Button');

    expect(button).toBeInTheDocument();
  });
});