import React, { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faGraduationCap, faPen, faGem, faBriefcase, faPaperPlane, faComment  } from '@fortawesome/free-solid-svg-icons'
import { Link, animateScroll as scroll } from "react-scroll";
import './Navigation.style.scss'

function Navigation() {
  const [isMobile, setMobile] = useState(window.innerWidth < 600);

  const updateMedia = () => {
    setMobile(window.innerWidth < 600);
  };

  useEffect(() => {
    window.addEventListener("resize", updateMedia);
    return () => window.removeEventListener("resize", updateMedia);
  });

  return (
    <ul className='navigation_list'>
      {isMobile ? (
        <>
          {[
            {
              name: 'About me',
              id: 'about_me',
              icon: faUser
            },
            {
              name: 'Education',
              id: 'education',
              icon: faGraduationCap
            },
            {
              name: 'Experience',
              id: 'experience',
              icon: faPen
            },
            {
              name: 'Portfolio',
              id: 'portfolio',
              icon: faBriefcase
            },
            {
              name: 'Contacts',
              id: 'contacts',
              icon: faPaperPlane
            },
            {
              name: 'Feedbacks',
              id: 'feedbacks',
              icon: faComment
            }].map((nav, index) => (
              <li className='navigation_item' key={nav.id}>
                <div className='navigation_link'>
                  <Link
                    activeClass="active"
                    to={nav.id}
                    spy={true}
                    smooth={true}
                    offset={1}
                    duration={500}
                    isDynamic={true}
                  >
                    <FontAwesomeIcon icon={nav.icon} />
                  </Link>
                </div>
              </li>
            ))}
        </>
      ) : (
        <>
          {[
            {
              name: 'About me',
              id: 'about_me',
              icon: faUser
            },
            {
              name: 'Education',
              id: 'education',
              icon: faGraduationCap
            },
            {
              name: 'Experience',
              id: 'experience',
              icon: faPen
            },
            {
              name: 'Portfolio',
              id: 'portfolio',
              icon: faBriefcase
            },
            {
              name: 'Contacts',
              id: 'contacts',
              icon: faPaperPlane
            },
            {
              name: 'Feedbacks',
              id: 'feedbacks',
              icon: faComment
            }].map((nav, index) => (
              <li className='navigation_item' key={nav.id}>
                <div className='navigation_link'>
                  <Link
                    activeClass="active"
                    to={nav.id}
                    spy={true}
                    smooth={true}
                    offset={1}
                    duration={500}
                    isDynamic={true}
                  >
                    <FontAwesomeIcon icon={nav.icon} />
                    <span className='navigation_link__text'>{nav.name}</span>
                  </Link>
                </div>
              </li>
            ))}
        </>
      )}
    </ul>
  );
}

export default Navigation;
