import React, { useEffect, useRef, useState } from 'react';
import type { RootState } from '../../store/index';
import { useSelector, useDispatch } from 'react-redux';
import { setFilter } from '../../store/features/portfolio/portfolioSlice';
import Isotope from 'isotope-layout';
import './Portfolio.style.scss'
import PortfolioInfo from '../PortfolioInfo/PortfolioInfo';


function Portfolio() {

  const [isAllActive, setAllActive] = useState(true);
  const [isUiActive, setUiActive] = useState(false);
  const [isCodeActive, setCodeActive] = useState(false);

  const portfolioFilter = useSelector((state: RootState) => state.portfolio.filter)
  const dispatch = useDispatch()

  const isotope: any = useRef();

  useEffect(() => {
    isotope.current = new Isotope('.filter-container', {
      itemSelector: '.filter-item',
      filter: `${portfolioFilter}`,
    })
    // cleanup
    return () => isotope.current.destroy()
  })
  

  function showAll() {
    setAllActive(current => {
      setUiActive(false)
      setCodeActive(false)

      return !current
    })
    dispatch(setFilter('*'))
  }

  function showUI() {
    setUiActive(current => {
      setAllActive(false)
      setCodeActive(false)

      return !current
    })
    dispatch(setFilter('.ui'))
  }

  function showCode() {
    setCodeActive(current => {
      setAllActive(false)
      setUiActive(false)

      return !current
    })
    dispatch(setFilter('.code'))
  }


    return (
      <div>
        <ul className="tabs">
          <li className={`${isAllActive ? 'active' : ''}`}>
            <span onClick={showAll}> all </span>
          </li>
          <li className={`${isUiActive ? 'active' : ''}`}>
            <span onClick={showUI}> ui </span>
          </li>
          <li className={`${isCodeActive ? 'active' : ''}`}>
            <span onClick={showCode}> code </span>
          </li>
        </ul>
        <ul className='filter-container'>
          <li className='filter-item ui'>
            <img src={require('../../assets/images/card_1.png')} alt="card" />
            <PortfolioInfo title="Some text" text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo" url="https://somesite.com" />
          </li>
          <li className='filter-item code'>
            <img src={require('../../assets/images/card_3.png')} alt="card" />
            <PortfolioInfo title="Some text" text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo" url="https://somesite.com" />
          </li>
          <li className='filter-item ui'>
            <img src={require('../../assets/images/card_1.png')} alt="card" />
            <PortfolioInfo title="Some text" text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo" url="https://somesite.com" />
          </li>
          <li className='filter-item code'>
            <img src={require('../../assets/images/card_3.png')} alt="card" />
            <PortfolioInfo title="Some text" text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo" url="https://somesite.com" />
          </li>
        </ul>
      </div>
    );
  }

export default Portfolio;