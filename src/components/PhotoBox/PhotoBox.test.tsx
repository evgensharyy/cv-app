import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import PhotoBox from './PhotoBox';

describe('<PhotoBox />', () => {
  test('it should mount', () => {
    render(<PhotoBox 
      name="John Doe"
      avatar="http://avatars0.githubusercontent.com/u/246180?v=4"
      sxWidth={100} 
      sxHeight={100}
      fontSize={12}
    />);
    
    const photoBox = screen.getByTestId('PhotoBox');

    expect(photoBox).toBeInTheDocument();
  });
});