import Avatar from '@mui/material/Avatar';
import React, { useEffect, useState } from 'react';
import './PhotoBox.style.scss'

interface PhotoBoxProps {
  name: string,
  title?: string,
  description?: string,
  avatar: string,
  sxWidth: number,
  sxHeight: number,
  fontSize: number,
  children?: JSX.Element | JSX.Element[];
}

function PhotoBox(props: PhotoBoxProps) {
  
  return (
    <div className='photoBox'>
      <Avatar className='photoBox-avatar' alt={props.name} src={props.avatar} sx={{ width: props.sxWidth, height: props.sxHeight }}/>
      {props.children}
    </div>
  );
}

export default PhotoBox;
