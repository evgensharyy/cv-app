import React from 'react';

interface PortfolioInfoProps {
  title: string,
  text: string,
  url: string
  children?: JSX.Element,
  className?: string
}

function PortfolioInfo(props: PortfolioInfoProps) {
  return (
    <div className='portfolio-info'>
      <h2 className='portfolio-info_title'>{props.title}</h2>
      <p className='portfolio-info_text'>{props.text}</p>
      <a className='portfolio-info_link' href={`${props.url}`}>View source</a>
    </div>
  );
}

export default PortfolioInfo;