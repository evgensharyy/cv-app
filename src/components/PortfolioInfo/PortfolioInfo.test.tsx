import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import PortfolioInfo from './PortfolioInfo';

describe('<PortfolioInfo />', () => {
  test('it should mount', () => {
    render(<PortfolioInfo title="Some text" text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo" url="https://somesite.com" />);
    
    const portfolioInfo = screen.getByTestId('PortfolioInfo');

    expect(portfolioInfo).toBeInTheDocument();
  });
});