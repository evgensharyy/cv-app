import React from 'react';
import Info from '../Info/Info';
import {ITimeline} from '../../models'
import './TimeLine.style.scss'

interface TimeLineProps {
  data: ITimeline[]
}



function TimeLine(props: TimeLineProps) {

  const listItems = props.data.map((data) => {

    return (
      <>
        <ul className="timeline-list">
          <li>
            <div className="timeline-date">{data.date}</div>
            <div className="general-event timeline-event">
              <Info text={data.text}>
                <h3>{data.title}</h3>
              </Info>
            </div>
          </li>
        </ul>
      </>
    );
  });

  return (
    <div>
      {listItems}
    </div>
  );
}

export default TimeLine;