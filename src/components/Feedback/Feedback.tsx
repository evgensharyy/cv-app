import React from 'react';
import styles from './Feedback.module.scss'
import {IFeedbacks} from '../../models'
import Info from '../Info/Info'
import Avatar from '@mui/material/Avatar'; 

interface FeedbackProps {
  photo?: string
}

const data: IFeedbacks[] = [
  {
    id: 1,
    text: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.', 
    cite: {
      name: 'somesite.com',
      description: 'A Game of Thrones',
      url: 'https://somesite.com/'
    }
  },
  {
    id: 2,
    text: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.', 
    cite: {
      name: 'somesite.com',
      description: 'A Game of Thrones',
      url: 'https://somesite.com/'
    }
  },
  {
    id: 3,
    text: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.', 
    cite: {
      name: 'somesite.com',
      description: 'A Game of Thrones',
      url: 'https://somesite.com/'
    }
  }
]

function Feedback(props: FeedbackProps) {

  const listItems = data.map((data) => {

    return (
      <>
        <div className={`${styles['info']}`}>
          <div className={`${styles['info_text']}`}>
            <Info text={data.text} />
          </div>
        </div>
        <div className={`${styles['reporter']}`}>
          <div className={`${styles['reporter_photo']}`}>
          <Avatar src={props.photo} />
          </div>
          <div className={`${styles['reporter_cite']}`}>
          <cite>{data.cite.description}, <a href={`${data.cite.url}`}>{data.cite.name}</a></cite>
          </div>
        </div>
      </>
    );
  });

  return (
    <div>{listItems}</div>
  );
}

export default Feedback;
