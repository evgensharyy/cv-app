import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Expertise from './Expertise';

const data = [
  {
    date: '2013-2014', 
    info: {
      company: 'Google',
      job: 'Front-end developer / php programmer',
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor'
    }
  },
  {
    date: '2012', 
    info: {
      company: 'Twitter',
      job: 'Web developer',
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor'
    }
  }
];

describe('<Expertise />', () => {
  test('it should mount', () => {
    render(<Expertise data={data} />);
    
    const expertise = screen.getByTestId('Expertise');

    expect(expertise).toBeInTheDocument();
  });
});