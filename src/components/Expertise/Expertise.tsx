import React from 'react';
import styles from './Expertise.module.scss'
import {IExpertise} from '../../models'

interface ExpertiseProps {
  data: IExpertise[]
}

function Expertise(props: ExpertiseProps) {
  const listItems = props.data.map((data) => {

    return (
      <li>
        <div className={`${styles['expertise_list-date']}`}>
          <h3>{data.info.company}</h3>
          <span className={`${styles.date}`}>{data.date}</span>
        </div>
        <div>
          <h3>{data.info.job}</h3>
          <p>{data.info.description}</p>
        </div>
      </li>
    );
  });

  return (
    <ul className={`${styles.expertise_list}`}>{listItems}</ul>
  );
}

export default Expertise;