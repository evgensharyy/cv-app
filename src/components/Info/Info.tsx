import React from 'react';
import './Info.style.scss'

interface InfoProps {
  text: string,
  children?: JSX.Element
}

function Info(props: InfoProps) {
  return (
    <div className='info'>
      {props.children}
      <p>{props.text}</p>
    </div>
  );
}

export default Info;