import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Info from './Info';


describe('<Info />', () => {
  test('it should mount', () => {
    render(<Info text="test"/>);
    
    const info = screen.getByTestId('Info');

    expect(info).toBeInTheDocument();
  });
});