import React from 'react';
import styles from './Box.module.scss'

interface BoxProps {
  title: string,
  content: string,
  id?: string
}

function Box(props: BoxProps) {
  return (
    <div id={props.id}>
      <h2>{props.title}</h2>
      <div className={`${styles.content}`}>{props.content}</div>
    </div>
  );
}

export default Box;