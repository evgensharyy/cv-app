import React, { useEffect, useState } from 'react';
import Button from '../Button/Button';
import Navigation from '../Navigation/Navigation';
import PhotoBox from '../PhotoBox/PhotoBox';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import styles from './Panel.module.scss'

function Panel() {
  const [isMobile, setMobile] = useState(window.innerWidth < 600);

  const updateMedia = () => {
    setMobile(window.innerWidth < 600);
  };

  useEffect(() => {
    window.addEventListener("resize", updateMedia);
    return () => window.removeEventListener("resize", updateMedia);
  });
  
  return (
    <div className={`${styles.panel}`}>
      {isMobile ? (
        <PhotoBox name="John Doe" avatar={require('../../assets/images/user-avatar.png')} sxWidth={40} sxHeight={40} fontSize={16}/>
      ) : (
        <PhotoBox name="John Doe" avatar={require('../../assets/images/user-avatar.png')} sxWidth={100} sxHeight={100} fontSize={16}>
          <h3 className='photoBox_name' style={{ fontSize: '16px'  }}>John Doe</h3>
        </PhotoBox>
      )}
      
      <Navigation />
    </div>
  );
}

export default Panel;