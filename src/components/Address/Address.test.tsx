import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Address from './Address';

describe('<Address />', () => {
  test('it should mount', () => {
    render(<Address />);
    
    const address = screen.getByTestId('Address');

    expect(address).toBeInTheDocument();
  });
});