import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { 
  faPhone, 
  faEnvelope, 
  faDove, 
  faThumbsUp, 
  faPhoneSquare 
} from '@fortawesome/free-solid-svg-icons'

import styles from './Address.module.scss'

interface AddressProps {}

function Address(props: AddressProps) {
  return (
    <address>
      <div className={`${styles['address-row']}`}>
        <div className={`${styles['address-row_icon']}`}>
          <FontAwesomeIcon icon={faPhone} />
        </div>
        <div className={`${styles['address-row_data']}`}>
          <a href="tel:+500 342 242">500 342 242</a>
        </div>
      </div>
      <div className={`${styles['address-row']}`}>
        <div className={`${styles['address-row_icon']}`}>
          <FontAwesomeIcon icon={faEnvelope} />
        </div>
        <div className={`${styles['address-row_data']}`}>
          <a href="mailto:office@kamsolutions.pl">office@kamsolutions.pl</a>
        </div>
      </div>
      <div className={`${styles['address-row']}`}>
        <div className={`${styles['address-row_icon']}`}>
          <FontAwesomeIcon icon={faDove} />
        </div>
        <div className={`${styles['address-row_data']}`}>
          <strong>Twitter</strong>
          <br /> 
          <a className={`${styles['cv-app-link']}`} href="https://twitter.com/wordpress">https://twitter.com/wordpress</a>
        </div>
      </div>
      <div className={`${styles['address-row']}`}>
      <div className={`${styles['address-row_icon']}`}>
          <FontAwesomeIcon icon={faThumbsUp} />
        </div>
        <div className={`${styles['address-row_data']}`}>
          <strong>Facebook</strong>
          <br /> 
          <a className={`${styles['cv-app-link']}`} href="https://www.facebook.com/facebook">https://www.facebook.com/facebook</a>
        </div>
      </div>
      <div className={`${styles['address-row']}`}>
      <div className={`${styles['address-row_icon']}`}>
          <FontAwesomeIcon icon={faPhoneSquare} />
        </div>
        <div className={`${styles['address-row_data']}`}>
          <strong>Skype</strong>
          <br /> 
          <a className={`${styles['cv-app-link']}`} href="skype:kamsolutions.pl">kamsolutions.pl</a>
        </div>
      </div>
    </address>
  );
}

export default Address;


