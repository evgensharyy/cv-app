export interface IInfo {
    company: string,
    job: string,
    description: string
}

export interface ICite {
    name: string,
    description: string,
    url: string
}

export interface IExpertise {
    date: string,
    info: IInfo
}

export interface IFeedbacks {
    id: number,
    text: string,
    cite: ICite
}

export interface ITimeline {
    date: number,
    title: string,
    text: string
}