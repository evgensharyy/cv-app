import React from 'react';
import { Routes, Route, Outlet, Link } from "react-router-dom";
import './App.css';
import Home from './pages/Home/Home'
import Inner from './pages/Inner/Inner'
import {Helmet} from "react-helmet";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="inner" element={<Inner />} />
      </Routes>
    </>
  );
}

export default App;
